package com.example.jhordan.equiporegaloperfecto.inicio;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jhordan.equiporegaloperfecto.R;
import com.example.jhordan.equiporegaloperfecto.mainDrawer.MyActivity;
import com.facebook.Session;
import com.sromku.simple.fb.Permission;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.SimpleFacebookConfiguration;
import com.sromku.simple.fb.entities.Profile;
import com.sromku.simple.fb.listeners.OnLoginListener;
import com.sromku.simple.fb.listeners.OnLogoutListener;
import com.sromku.simple.fb.listeners.OnProfileListener;

public class LoginActivity extends ActionBarActivity {

    private String TAG="";

    private Session current;

    private Button btnFbLogin;
    private Button btnFbLogout;

    private TextView mTextStatus;

    private SimpleFacebook mSimpleFacebook;

    Permission[] permissions = new Permission[]{
            Permission.USER_PHOTOS,
            Permission.USER_BIRTHDAY,
            Permission.FRIENDS_BIRTHDAY,
            Permission.PUBLISH_ACTION,
            Permission.FRIENDS_LOCATION,
            Permission.FRIENDS_PHOTOS,
            Permission.FRIENDS_ABOUT_ME,
            Permission.FRIENDS_LIKES,
            Permission.FRIENDS_BIRTHDAY

    };

    /*Configuraciones necesarias para*/
    SimpleFacebookConfiguration configuration = new SimpleFacebookConfiguration.Builder()
            .setAppId("1456281004629889")
            .setNamespace("aseregaloperfecto")
            .setPermissions(permissions)
            .build();

    Profile.Properties properties = new Profile.Properties.Builder()
            .add(Profile.Properties.ID)
            .add(Profile.Properties.FIRST_NAME)
            .add(Profile.Properties.BIRTHDAY)
            .add(Profile.Properties.EMAIL)
            .add(Profile.Properties.COVER)
            .add(Profile.Properties.WORK)
            .add(Profile.Properties.EDUCATION)
            .add(Profile.Properties.PICTURE)
            .build();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.screen_login);

        SimpleFacebook.setConfiguration(configuration);

        mSimpleFacebook = SimpleFacebook.getInstance(this);
        current = Session.getActiveSession();

        btnFbLogin = (Button) this.findViewById(R.id.button);
        btnFbLogout = (Button) this.findViewById(R.id.button2);

        mTextStatus = (TextView) this.findViewById(R.id.textView3);

        this.setLogin();
        this.setLogout();

        this.setUIState();
    }

    private void setUIState() {

        if (mSimpleFacebook.isLogin()) {
            loggedInUIState();
        } else {
            loggedOutUIState();
        }
    }

    private void setLogout() {
        final OnLogoutListener onLogoutListener = new OnLogoutListener() {

            @Override
            public void onFail(String reason) {
                mTextStatus.append(reason);
                Log.w(TAG, "Failed to login");
            }

            @Override
            public void onException(Throwable throwable) {
                mTextStatus.setText("Exception: " + throwable.getMessage());
                Log.e(TAG, "Bad thing happened", throwable);
            }

            @Override
            public void onThinking() {
                // show progress bar or something to the user while login is
                // happening
                mTextStatus.setText("Thinking...");
            }

            @Override
            public void onLogout() {
                // change the state of the button or do whatever you want
                mTextStatus.setText("Logged out");
                loggedOutUIState();
            }

        };

        btnFbLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Log.i("Click", "logout");
                mSimpleFacebook.logout(onLogoutListener);
            }
        });
    }

    private void setLogin() {
        // Login listener
        final OnLoginListener onLoginListener = new OnLoginListener() {

            @Override
            public void onFail(String reason) {
                mTextStatus.setText(reason);
                Log.w(TAG, "Failed to login");
            }

            @Override
            public void onException(Throwable throwable) {
                mTextStatus.setText("Exception: " + throwable.getMessage());
                Log.e(TAG, "Bad thing happened", throwable);
            }

            @Override
            public void onThinking() {
                // show progress bar or something to the user while login is
                // happening
                mTextStatus.setText("Thinking...");
            }

            @Override
            public void onLogin() {
                // change the state of the button or do whatever you want
                mTextStatus.setText("Logged in");
                loggedInUIState();
            }

            @Override
            public void onNotAcceptingPermissions(Permission.Type type) {
//				toast(String.format("You didn't accept %s permissions", type.name()));
            }
        };

        btnFbLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Log.i("Click", "login");
                mSimpleFacebook.login(onLoginListener);
            }
        });
    }

    /*Se ejecuta cuando estamos en loggedIn*/
    protected void loggedInUIState() {

        Log.i("Logged", "In");

        btnFbLogin.setEnabled(false);
        btnFbLogout.setEnabled(true);
        mTextStatus.setText("Logged in");

		/*Obtenemos profile*/
        mSimpleFacebook.getProfile(properties,new OnProfileListener() {
            @Override
            public void onComplete(Profile profile) {
                Log.e("bithday", "" + profile.getBirthday());
                if (profile.getBirthday() == "null") {

                    /*DialogDatePicker dialogDatePicker = new DialogDatePicker();
                    dialogDatePicker.show(getSupportFragmentManager(), "Tag");*/
                    Toast.makeText(getBaseContext(), "Mi nombre: " + profile.getFirstName() + " Mi cumpleaños: " + profile.getBirthday()+" Mi id:"+profile.getId()+" Mi email: "+profile.getEmail()+" Mi foto: "+profile.getPicture(), Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getBaseContext(), "Mi nombre: " + profile.getFirstName() + " Mi cumpleaños: " + profile.getBirthday()+" Mi id:"+profile.getId()+" Mi email: "+profile.getEmail()+" Mi foto: "+profile.getPicture(), Toast.LENGTH_LONG).show();
                }
            }
        });

        Log.i("Acces token",current.getAccessToken());
        this.goHome();
    }

    /*Se ejecuta cuando estamos en loggedOut*/
    protected void loggedOutUIState() {
        Log.i("Logged", "out");
        btnFbLogin.setEnabled(true);
        btnFbLogout.setEnabled(false);
        mTextStatus.setText("Logged out");

    }

    private void hideActionbar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();
    }


    @Override
    public void onResume() {
        super.onResume();
        mSimpleFacebook = SimpleFacebook.getInstance(this); /*Obtenemos de nuevo la instancia del objeto */
        current = Session.getActiveSession();
    }

    @Override
    public void onPause() {
        super.onPause();


    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

    }


    private void goHome() {

        Intent intento = new Intent(LoginActivity.this, MyActivity.class);
        startActivity(intento);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        mSimpleFacebook.onActivityResult(this, requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
        current = Session.getActiveSession();
    }






}
