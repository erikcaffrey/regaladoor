package com.example.jhordan.equiporegaloperfecto.util;

/**
 * Created by Jhordan on 20/07/14.
 */
public class UserData {

    private String email;



    public UserData(String email){
        this.email = email;

    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
