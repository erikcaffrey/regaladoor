package com.example.jhordan.equiporegaloperfecto.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.jhordan.equiporegaloperfecto.R;
import com.example.jhordan.equiporegaloperfecto.util.FacebookFriends;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jhordan on 22/07/14.
 */
public class FriendsAdapter extends BaseAdapter {

    private List<FacebookFriends> friends;
    private Context mContext;
    private LayoutInflater layoutInflater;

    public FriendsAdapter(List<FacebookFriends> friends, Context mContext) {
        this.friends = friends;
        this.mContext = mContext;
        layoutInflater = LayoutInflater.from(mContext);

    }

    public FriendsAdapter(Context mContext) {
        this.mContext = mContext;
        this.friends = new ArrayList<FacebookFriends>();
        layoutInflater = LayoutInflater.from(mContext);

    }

    @Override
    public int getCount() {
        return friends.size();
    }

    @Override
    public Object getItem(int position) {
        return friends.get(position);
    }

    @Override
    public long getItemId(int position) {
        return friends.indexOf(friends.get(position));
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        PropertyHolderView propertyHolderView;
        View container = convertView;
        if(container == null){
            propertyHolderView = new PropertyHolderView();
            container = layoutInflater.inflate(R.layout.friends_custom,parent,false);
            propertyHolderView.initViewsFromIds(container, R.id.friend_photo,R.id.friends_name,R.id.friend_birthday);
            container.setTag(propertyHolderView);
        }else{
            propertyHolderView = (PropertyHolderView)container.getTag();
        }


        propertyHolderView.getFriendImageContainer().setImageResource(R.drawable.ic_launcher);
        propertyHolderView.getFriendNameContainer().setText("Erik Jhordan Gonzalez Reyes");
        propertyHolderView.getFriendBirthdayContainer().setText("22-12-1993");




        /*
         PropertyModel propertyModel = properties.get(position);

           propertyHolderView.getPropertyPriceContainer()
                   .setText(propertyModel.getPrice());

           propertyHolderView.getPropertyInfoContainer().
                   setText(propertyModel.getBedrooms() + "hab, "
                           + propertyModel.getBathrooms() + "baños, "
                           + propertyModel.getPropertySize());

          propertyHolderView.getPropertyAddressContainer().
                  setText(propertyModel.getAddress());

        Picasso.with(mContext)
                .load(propertyModel.getPropertyImageUrl())
                .into(propertyHolderView.getPropertyImageContainer());

    */

        return container;
    }



    private class PropertyHolderView{

        private ImageView friend_photo;
        private TextView friend_name;
        private TextView friend_birthday;


        public PropertyHolderView(){

        }

        public void initViewsFromIds(View container,int friend_photo ,int friends_name ,int friend_birthday){

            this.friend_photo = (ImageView)container.findViewById(friend_photo);
            this.friend_name = (TextView)container.findViewById(friends_name);
            this.friend_birthday = (TextView)container.findViewById(friend_birthday);

        }

        public ImageView getFriendImageContainer() {
            return friend_photo;
        }

        public TextView getFriendNameContainer() {
            return friend_name;
        }

        public TextView getFriendBirthdayContainer() {
            return friend_birthday;
        }



    }
}
