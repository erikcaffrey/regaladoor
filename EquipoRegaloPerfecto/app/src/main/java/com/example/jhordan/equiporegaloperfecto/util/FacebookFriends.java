package com.example.jhordan.equiporegaloperfecto.util;

/**
 * Created by Jhordan on 21/07/14.
 */
public class FacebookFriends {

    private String Friend_name;
    private String Friend_birthday;
    private String Friend_picture;

    public FacebookFriends(){

    }
    public FacebookFriends(String Friend_name , String Friend_birthday,String Friend_picture){

        this.Friend_name = Friend_name;
        this.Friend_birthday = Friend_birthday;
        this.Friend_picture = Friend_picture;

    }

    public String getFriend_name() {
        return Friend_name;
    }

    public void setFriend_name(String friend_name) {
        Friend_name = friend_name;
    }

    public String getFriend_birthday() {
        return Friend_birthday;
    }

    public void setFriend_birthday(String friend_birthday) {
        Friend_birthday = friend_birthday;
    }

    public String getFriend_picture() {
        return Friend_picture;
    }

    public void setFriend_picture(String friend_picture) {
        Friend_picture = friend_picture;
    }
}
