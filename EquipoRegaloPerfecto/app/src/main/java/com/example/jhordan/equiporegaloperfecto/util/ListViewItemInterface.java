package com.example.jhordan.equiporegaloperfecto.util;

/**
 * Created by Jhordan on 20/07/14.
 */
public interface ListViewItemInterface {
    public boolean isSection();
}
