package com.example.jhordan.equiporegaloperfecto.otto;

import com.squareup.otto.Bus;

/**
 * Created by Jhordan on 20/07/14.
 */
public final class BusProvider {

    private static final Bus BUS = new Bus();

    public static Bus getInstance() {
        return BUS;
    }

    private BusProvider() {
        // No instances.
    }

}
