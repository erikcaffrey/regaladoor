package fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.jhordan.equiporegaloperfecto.R;
import com.example.jhordan.equiporegaloperfecto.mainDrawer.MyActivity;
import com.example.jhordan.equiporegaloperfecto.mainDrawer.NavigationDrawerFragment;

/**
 * Created by Jhordan on 22/07/14.
 */
public class MyCumpleProximo extends Fragment {

    public MyCumpleProximo() {
    }

    public static MyCumpleProximo newInstance(int position) {

       MyCumpleProximo cumples = new MyCumpleProximo();
        Bundle extraArguments = new Bundle();
        extraArguments.putInt(NavigationDrawerFragment.ARG_SECTION_NUMBER, position);
        cumples.setArguments(extraArguments);
        return cumples;
    }
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_cumples, container, false);


        return v;

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MyActivity) activity).onSectionAttached(getArguments()
                .getInt(NavigationDrawerFragment.ARG_SECTION_NUMBER));
    }

}
