package fragments;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.jhordan.equiporegaloperfecto.R;
import com.example.jhordan.equiporegaloperfecto.adapter.FriendsAdapter;
import com.example.jhordan.equiporegaloperfecto.mainDrawer.NavigationDrawerFragment;

/**
 * Created by Jhordan on 22/07/14.
 */

public class MyFriends extends android.support.v4.app.Fragment {

    public MyFriends() {
    }

    public static MyFriends newInstance(int position) {

        MyFriends myfriends = new MyFriends();
        Bundle extraArguments = new Bundle();
        extraArguments.putInt(NavigationDrawerFragment.ARG_SECTION_NUMBER, position);
        myfriends.setArguments(extraArguments);
        return myfriends;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_myfriends, container, false);


        return v;

    }


}