package fragments;

import android.annotation.TargetApi;
import android.app.Activity;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.example.jhordan.equiporegaloperfecto.R;
import com.example.jhordan.equiporegaloperfecto.mainDrawer.MyActivity;
import com.example.jhordan.equiporegaloperfecto.mainDrawer.NavigationDrawerFragment;

/**
 * Created by Jhordan on 22/07/14.
 */

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class MyWishes extends android.support.v4.app.Fragment {


    public MyWishes() {
    }

    public static MyWishes newInstance(int position) {

        MyWishes mywishes = new MyWishes();
        Bundle extraArguments = new Bundle();
        extraArguments.putInt(NavigationDrawerFragment.ARG_SECTION_NUMBER, position);
        mywishes.setArguments(extraArguments);
        return mywishes;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_mywishes, container, false);


        return v;

    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MyActivity) activity).onSectionAttached(getArguments()
                .getInt(NavigationDrawerFragment.ARG_SECTION_NUMBER));
    }


}
