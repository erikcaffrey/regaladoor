package fragments;


import android.app.Activity;

import android.os.Bundle;
import android.support.v4.app.ListFragment;

import android.support.v7.widget.SearchView;
import android.support.v4.app.ListFragment;
import android.support.v4.view.MenuItemCompat;
import com.example.jhordan.equiporegaloperfecto.adapter.FriendsAdapter;
import com.example.jhordan.equiporegaloperfecto.mainDrawer.MyActivity;
import com.example.jhordan.equiporegaloperfecto.mainDrawer.NavigationDrawerFragment;
import com.example.jhordan.equiporegaloperfecto.util.FacebookFriends;


import java.util.ArrayList;
import java.util.List;


/**
 * Created by Jhordan on 22/07/14.
 */
public class Home extends ListFragment {
    private SearchView searchView;

    public Home() {
    }

    public static Home newInstance(int position) {

        Home homeFragment = new Home();
        Bundle extraArguments = new Bundle();
        extraArguments.putInt(NavigationDrawerFragment.ARG_SECTION_NUMBER, position);
        homeFragment.setArguments(extraArguments);
        return homeFragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public void onResume() {
        super.onResume();

        List<FacebookFriends> MyFriends = new ArrayList<FacebookFriends>();
        FriendsAdapter FriendAdapter = new FriendsAdapter(MyFriends, getActivity());

        for (int index = 0; index < 10; index++) {
            FacebookFriends facebookModel = new FacebookFriends();
            MyFriends.add(facebookModel);

        }

        setListAdapter(FriendAdapter);

    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MyActivity) activity).onSectionAttached(getArguments()
                .getInt(NavigationDrawerFragment.ARG_SECTION_NUMBER));
    }

}
